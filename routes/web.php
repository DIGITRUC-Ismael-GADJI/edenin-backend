<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function () {
    dd(url('/storage/edenin/images/5/2019-06-08-pic-page.jpg'));
});

Route::group(['prefix' => 'api'], function (){
    Route::resource('ages', 'AgeController');
    Route::resource('users', 'UserController');
    Route::resource('livres', 'LivreController');
    Route::resource('videos', 'VideoController');
    Route::post('achat-livre', 'LivreController@achatLivre');
    Route::resource('textes', 'TexteController');
    Route::resource('langues', 'LangueController');
    Route::resource('auteurs', 'AuteurController');
    Route::resource('contenus', 'ContenuController');
    Route::resource('partages', 'PartageController');
    Route::resource('progressions', 'ProgressionController');
    Route::resource('categories', 'CategorieController');
    Route::resource('dispositions', 'DispositionController');
    Route::resource('images', 'ImageController');
});

/**
 * Route::get('{any?}', function () {
\Illuminate\Support\Facades\View::addExtension('html', 'php');
return \Illuminate\Support\Facades\View::make('index');
})->where('any', '.*');
 */
