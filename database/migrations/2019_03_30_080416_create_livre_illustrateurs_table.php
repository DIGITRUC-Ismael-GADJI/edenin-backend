<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLivreIllustrateursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('livre_illustrateurs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('illustrateur_id')->unsigned()->nullable();
            $table->foreign('illustrateur_id')->references('id')->on('illustrateurs');
            $table->integer('livre_id')->unsigned()->nullable();
            $table->foreign('livre_id')->references('id')->on('livres');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('livre_illustrateurs');
    }
}
