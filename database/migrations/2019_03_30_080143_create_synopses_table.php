<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSynopsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('synopses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('contenu');

            $table->integer('langue_id')->unsigned()->nullable();
            $table->foreign('langue_id')->references('id')->on('langues');

            $table->integer('livre_id')->unsigned()->nullable();
            $table->foreign('livre_id')->references('id')->on('livres');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('synopses');
    }
}
