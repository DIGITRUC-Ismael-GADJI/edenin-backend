<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgressionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('progressions', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('livre_id')->unsigned();
            $table->foreign('livre_id')->references('id')->on('livres');


            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('contenu_id')->unsigned();
            $table->foreign('contenu_id')->references('id')->on('contenus');

            $table->integer('langue_id')->unsigned();
            $table->foreign('langue_id')->references('id')->on('langues');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('progressions');
    }
}
