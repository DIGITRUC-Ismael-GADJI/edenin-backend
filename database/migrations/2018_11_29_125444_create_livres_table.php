<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLivresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('livres', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titre');
            $table->string('picture', 500);
            $table->boolean('etat')->default(0);
            $table->boolean('delete')->default(0);
            $table->integer('age_id')->unsigned();
            $table->foreign('age_id')->references('id')->on('ages');
            $table->timestamps();
        });
    }
    //2018_11_29_125444_create_livres_table.php
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('livres');
    }
}
