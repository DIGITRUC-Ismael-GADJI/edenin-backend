<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Synopsis extends Model
{
    public function langue() {
        return $this->belongsTo('App\Langue');
    }
}
