<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriesHasVideo extends Model
{
    public function videos() {
        return $this->belongsToMany('App\Video', 'categories_has_videos');
    }
}
