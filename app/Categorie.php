<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    public function livres($idCategory) {
        $valeurs = explode(",", $idCategory);
        $result = array();
        foreach ($valeurs as $valeur){
            $category = Categorie::where('valeur_client', $valeur)->first();
            $listes = LivresHasCategorie::where('categorie_id', $category->id)->get();
            foreach ($listes as $element){
                $data = array(
                    'livre'     => Livre::find($element->livre_id)->toArray(),
                    'auteurs'   => Livre::auteurs($element->livre_id),
                    'categories'  => Livre::categories($element->livre_id),
                );
                array_push($result, $data);
            }
        }
        return $result;
    }


    public function videos() {
        return $this->belongsToMany('App\Video', 'categories_has_videos', 'category_id', 'video_id');
    }
}
