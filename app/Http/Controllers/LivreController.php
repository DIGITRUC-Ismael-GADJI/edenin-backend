<?php

namespace App\Http\Controllers;

use App\Age;
use App\Auteur;
use App\Categorie;
use App\Contenu;
use App\Disposition;
use App\Illustrateur;
use App\Image;
use App\Langue;
use App\Livre;
use App\LivreIllustrateur;
use App\LivresHasAge;
use App\LivresHasAuteur;
use App\LivresHasCategorie;
use App\Synopsis;
use App\Texte;
use App\UsersHasLivre;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Mockery\CountValidator\Exception;

class LivreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //Récupération d'un livre en fonction d'une catégorie
        if($request->category && is_null($request->age)){
            $category = new Categorie();
            $livres = $category->livres($request->category);
            return response()->json($livres, 201);
        }

        // Récupération des livres en fonction de l'age
        if($request->age && is_null($request->category)){
            $livres = Livre::where('age_id', $request->age)->get()->toArray();
            $result = array();
            foreach ($livres as $livre) {
                array_push($result, array(
                    'livre'     => Livre::find($livre->id)->toArray(),
                    'auteurs'   => Livre::auteurs($livre->id),
                    'categories'  => Livre::categories($livre->id),
                ));
            }
            return response()->json($result, 201);
        }

        //Recupération des livres suivant une catégorie et une tranche d'age
        if($request->category && $request->age){
            $category = new Categorie();
            $livres = $category->livres($request->category);
            $result = array();
            foreach ($livres as $livre) {
                if($livre->age_id == $request->age) {
                    array_push($result, array(
                        'livre'     => Livre::find($livre->id)->toArray(),
                        'auteurs'   => Livre::auteurs($livre->id),
                        'categories'  => Livre::categories($livre->id),
                    ));
                }
            }
            return response()->json($result, 201);
        }

        //Recupération des livres et des informations
        $livres = Livre::where('delete', 0)->get();
        $results = array();
        foreach ($livres as $livre){
            array_push($results, array(
                'livre' => $livre,
                'age'   => Age::find($livre->age_id),
                'description'   => Synopsis::where('livre_id', $livre->id)->with('langue')->get(),
                'categories'    => Livre::categories($livre->id)
            ));
        }
        return response()->json(array('livres' => $results), 201);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /**
         * $name = 'edenin/'. Carbon::now()->toDateString().'-pic-livre-'. $request->titre. '.jpg';
        Storage::disk('spaces')->put($name, base64_decode($request->picture), "public");

        $image = new Contenu();
        $image->contenu_id = $request->contenu_id;
        $image->langue_id = $request->langue_id;
        $image->url = Storage::disk('spaces')->url($name);
        $image->save();
        return response()->json(array(
        'message'   => 'Texte ajouté au contenu',
        'image'     => Storage::disk('spaces')->url($name)
        ), 201);
         */

        DB::beginTransaction();
        try{
            $name = 'edenin/images/'. Carbon::now()->toDateString().'-pic-livre-'. $request->titre. '.jpg';
            Storage::disk('public')->put($name, base64_decode($request->picture));
            $livre = new Livre();
            $livre->titre = $request->titre;
            $livre->etat = intval($request->etat);
            $livre->picture = $name;
            $livre->save();

            //Ajout de l'image de préface du livre dans la table
            $image = new Image();
            $image->url = $name;
            $image->position = 0;
            $image->save();

            foreach ($request->categories as $category) {
                $livreHasCategorie = new LivresHasCategorie();
                $livreHasCategorie->livre_id = $livre->id;
                $livreHasCategorie->categorie_id = $category;
                $livreHasCategorie->save();
            }

            /**
             * foreach ($request->ages as $age) {
                $livreHasAge = new LivresHasAge();
                $livreHasAge->livre_id = $livre->id;
                $livreHasAge->age_id = $age;
                $livreHasAge->save();
                }
             */

            foreach ($request->auteurs as $auteur) {
                $livreHasAuteur = new LivresHasAuteur();
                $livreHasAuteur->livre_id = $livre->id;
                $livreHasAuteur->auteur_id = $auteur;
                $livreHasAuteur->save();
            }

            //Ajout des illustrateurs
            foreach ($request->illustrateurs as $illustrateur) {
                $element = new LivreIllustrateur();
                $element->livre_id = $livre->id;
                $element->illustrateur_id = $illustrateur;
                $element->save();
            }

            //Ajout de la description par rapport à la langue
            $element = new Synopsis();
            $element->contenu = $request->description;
            $element->livre_id = $livre->id;
            $element->langue_id = $request->langue;
            $element->save();

            DB::commit();
            return response()->json(array(
                'message'   => 'Le livre a été enregistré. Merci'
            ), 201);
        }catch (Exception $e){
            DB::rollBack();
            return response()->json(array(
                'message'   => "Un problème est survénu durant l'enregistrement"
            ), 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $livre = Livre::find($id);
        $contenus = $livre->contenus($request->langue)->orderBy('position', 'asc')->get()->toArray();
        $langues = Livre::getAllLangageForThisBook($id);

        $contentByLanguage = array();
        foreach ($langues as $langue) {
            $temp = $livre->contenus($langue->id)->orderBy('position', 'asc')->get()->toArray();
            $contentByLanguage[$langue->id][] = $temp;
        }
        $data = array(
            'livre'         => Livre::find($id),
            'description'   => Synopsis::where('livre_id', $livre->id)->with('langue')->get(),
            'contenus'      => $contenus,
            'age'           => Age::find($livre->age_id),
            'auteurs'       => Livre::auteurs($id),
            'langues'       => $langues, //Langues disponibles pour le livre
            'contentByLanguage'       => $contentByLanguage, //Dispositions des contenus par langues
            'categories'    => Livre::categories($id),
            'dispositions'  => Disposition::all(),
            'illustrateurs' => Livre::illustrateurs($id),
            'allImages'     => Image::all()
        );
        return response()->json($data, 201);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $livre = Livre::find($id);
        $livre->delete = true;
        $livre->save();
        $request = new Request();
        return $this->index($request);
    }


    public function achatLivre(Request $request) {
        $userHasLivre = new UsersHasLivre();
        $userHasLivre->user_id = $request->user_id;
        $userHasLivre->livre_id = $request->livre_id;
        $userHasLivre->save();

        //Partage du livre  aux membre de la famille.
        return response()->json(array(
            'message'   => "Merci pour votre achat et bonne lecture."
        ), 201);
    }



    private function getAllBooks() {
        $livres = Livre::where('delete', 0)->get();
        $result = array();
        foreach ($livres as $book) {
            array_push($result, array(
                'livre' => $book,
                'categories'    => $book->categories()->get()->toArray(),
                'ages'          => $book->ages()->get()->toArray()
            ));
        }
        return $result;
    }
}
