<?php

namespace App\Http\Controllers;

use App\Categorie;
use App\CategoriesHasVideo;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PHPUnit\Runner\Exception;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $videos = Video::where('delete', 0)->with('categories')->get();
        if($request->category){
            $videoCategoery = CategoriesHasVideo::where('category_id', $request->category)->get();
            foreach ($videoCategoery as $element) {
                array_push($videos, Video::where('id', $element->video_id)->where('delete', 0)
                    ->where('publier', 1)->first());
            }
        }
        return response()->json(array(
            'message'   => 'Liste des vidéos',
            'videos'    => $videos,
            'categories'=> Categorie::all()
        ), 201);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try{
            $video = new Video();
            $video->description = $request->description;
            $video->url = $request->url;
            $video->titre = $request->titre;
            $video->publier = intval($request->publier);
            $video->save();
            foreach ($request->categories as $category) {
                $videoHasCategorie = new CategoriesHasVideo();
                $videoHasCategorie->video_id = $video->id;
                $videoHasCategorie->category_id = $category;
                $videoHasCategorie->save();
            }
            DB::commit();
            return response()->json(array(
                'message'   => 'Vidéo ajoutée',
                'video' => Video::where('id', $video->id)->with('categories')->first()
            ), 201);
        }
        catch (Exception $e){
            DB::rollBack();
            Log::error($e->getMessage());
            return response()->json(array('message' => 'Un problème est survénu. Veuillez réessayer dans un instant'), 301);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $video = Video::find($id);
        return response()->json(array('video' => $video), 201);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try{
            $video = Video::find($id);
            $video->description = $request->description;
            $video->url = $request->url;
            $video->titre = $request->titre;
            $video->publier = intval($request->publier);
            $video->save();

            //Suppression des anciennes catégorie
            foreach ($request->allCategories as $element) {
                CategoriesHasVideo::where('video_id', $element['pivot']['video_id'])->delete();
            }

            //Enregistrement des nouvelles catégories
            foreach ($request->categories as $category) {
                $videoHasCategorie = new CategoriesHasVideo();
                $videoHasCategorie->video_id = $video->id;
                $videoHasCategorie->category_id = $category;
                $videoHasCategorie->save();
            }

            DB::commit();
            return response()->json(array(
                'video' => $video,
                'message'   => 'Mise à jour effectuée'
            ), 201);
        }
        catch (Exception $e){
            DB::rollBack();
            return response()->json(array('Un problème est survénu. Veuillez réessayer dans un instant'), 301);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $video = Video::find($id);
            $video->delete = true;
            $video->save();
            return response()->json(array('message'=> 'Vidéo supprimée'), 201);
        }
        catch (Exception $e) {
            return response()->json(array('message' => 'Un problème est survénu. Veuilez réessayer dans un instant'), 301);
        }
    }
}
