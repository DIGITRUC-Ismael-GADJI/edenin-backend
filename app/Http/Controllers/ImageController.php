<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use PHPUnit\Runner\Exception;

class ImageController extends Controller
{
    public function index(){}

    public function store(Request $request) {
        DB::beginTransaction();
        try {
            $name = 'edenin/images/'. $request->contenu_id . '/' . Carbon::now()->toDateString().'-pic-page.jpg';
            Storage::disk('public')->put($name, base64_decode($request->picture));
            $image = new Image();
            $image->contenu_id = $request->contenu_id;
            $image->langue_id = $request->langue_id;
            $image->url = $name;
            $image->position = $request->position;
            $image->save();
            DB::commit();
            return response()->json(array(
                'message'   => 'Image ajoutée avec succès',
                'image'     => url($name)
            ), 201);
        }
        catch (Exception $e) {
            DB::rollBack();
            Log::info($e->getMessage());
        }
    }

    public function update(Request $request, $id) {
        try{
            $image = Image::find($id);
            $name = 'edenin/images/'. $image->contenu_id . '/' . Carbon::now()->toDateString().'-pic-page.jpg';
            Storage::disk('public')->put($name, base64_decode($request->image));
            $image->position = $request->position;
            $image->url = Storage::disk('public')->url($name);
            $image->save();
            return response()->json(array('message' => 'Mise à jour effectuée'), 201);
        }
        catch (Exception $e){
            return response()->json(array('message' => 'Un problème est survénu. Veuillez réessayer dans un instant'), 301);
        }
    }

    public function show($id) {}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        try{
            Image::destroy($id);
            return response()->json(array('message' => 'Cette image à été supprimée'), 201);
        }
        catch (Exception $e){
            return response()->json(array('message' => 'Un problème est survénu. Veuillez réessayer dans un instant'), 301);
        }

    }


}
