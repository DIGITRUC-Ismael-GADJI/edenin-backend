<?php

namespace App\Http\Controllers;

use App\Contenu;
use App\Image;
use App\Langue;
use App\Livre;
use App\Texte;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mockery\CountValidator\Exception;

class ContenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //Récupération des contenus par langues
    public function index(Request $request)
    {
        $langue = Langue::where('nom', $request->langue)->first();
        //Recupération des contenus d'un livre et d'une langues
        $contenus = Contenu::where('langue_id', $langue->id)
            ->where('livre_id', $request->livre_id)
            ->with('disposition')
            ->get();

        //Recupération des elements de chaque contenu
        $result = array();
        foreach ($contenus as $contenu) {
            array_push($result, array(
                'contenu'   => $contenu,
                'texte'     => Texte::where('contenu_id', $contenu->id)->get(),
                'image'     => $this->retrieveImages($contenu->id),
                'disposition'   => $contenu->disposition->toArray()
            ));
        }
        return response()->json($result, 201);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contenu = new Contenu();
        $contenu->livre_id = $request->livre_id;
        $contenu->position = $request->position;
        $contenu->langue_id = $request->langue_id;
        $contenu->disposition_id = $request->disposition_id;
        $contenu->save();

        $livre = Livre::find($request->livre_id);
        $contenus = $livre->contenus($request->langue)->orderBy('position', 'asc')->get()->toArray();

        return response()->json(array(
            'message'   => 'Nouveau contenu ajouté à ce livre',
            'contenus'   => $contenus
        ), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $langue = Langue::where('nom', $request->langue)->first();
        //Recupération des contenus d'un livre et d'une langues
        $contenu = Contenu::where('langue_id', $langue->id)
            ->where('livre_id', $request->livre_id)
            ->where('delete', false)
            ->with('disposition')
            ->get();

        //Recupération des elements de chaque contenu
        $result = array();
        array_push($result, array(
            'contenu'   => $contenu,
            'texte'     => Texte::where('contenu_id', $id)->get(),
            'image'     => $this->retrieveImages($id),
            'disposition'   => $contenu->disposition->toArray()
        ));
        return response()->json($result, 201);
    }

    private function retrieveImages($idContenu) {
        $result = array();
        $images = Image::where('contenu_id', $idContenu)->get();
        foreach ($images as $image) {
            //array_push($result, Storage::disk('spaces')->url($image->name));
            array_push($result, url($image->name));
        }
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        //Mise à jour de la position d'un élément
        if(!is_null($request->update_position)){
            $contenus = Contenu::where('position', '>=', intval($request->position))
                                ->where('livre_id', $request->livre)
                                ->where('langue_id', $request->langueId)
                                ->orderBy('position', 'asc')
                                ->get();

            Log::info($request);
            Log::info("ICI");
            Log::info($contenus);

            //Mise à jour de l'élément actuel
            DB::beginTransaction();
            try{
                $contenu = Contenu::find($id);
                $contenu->position = $request->position;
                $contenu->save();

                //Deplacement des autres éléments
                foreach ($contenus as $value){
                    $temp= Contenu::find($value->id);
                    $temp->position = $request->position + 1;
                    $temp->save();
                }
                DB::commit();
            }catch (Exception $e){
                DB::rollBack();
                return response()->json(array(
                    'message'   => 'Un problème est survénu durant la mise à jour'), 301);
            }

            return response()->json(array(
                'message'   => 'Mise à jour effectuée.'), 201);
        }

        $contenu = Contenu::find($id);
        $contenu->livre_id = $request->livre_id;
        $contenu->langue_id = $request->langue_id;
        $contenu->disposition_id = $request->disposition_id;
        $contenu->save();

        $temp = Contenu::find($contenu->id);
        $temp->position = $temp->id;
        return response()->json(array(
            'message'   => 'Mise à jour effectuée.',
            'contenu'   => $temp
        ), 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      try{
        $content = Contenu::find($id);
        $content->delete = true;
        $content->save();

        return response()->json(array(
            'message'    => 'Ce contenu à été supprimé.'
        ), 201);
      }
      catch(Exception $e){
        return response()->json(array(
            'message'    => 'Un problème est survénu durant la suppression.'
        ), 301);
      }

    }
}
