<?php

namespace App\Http\Controllers;

use App\Texte;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use PHPUnit\Runner\Exception;

class TexteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $texte = new Texte();
        $texte->description = $request->description;
        $texte->contenu_id = $request->contenu_id;
        $texte->langue_id = $request->langue_id;
        $texte->position = $request->position;
        $texte->save();
        return response()->json(array(
            'message'   => 'Texte ajouté au contenu',
            'texte'     => $texte
        ), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $texte = Texte::find($id);
            $texte->position = $request->position;
            $texte->description = $request->description;
            $texte->save();
            return response()->json(array('message' => 'Mise à jour effectuée'), 201);
        }
        catch (Exception $e){
            Log::error($e->getMessage());
            return response()->json(array('message' => 'Un problème est survénu. Veuillez réessayer dans un instant'), 301);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            Texte::destroy($id);
            return response()->json(array('message' => 'Ce texte à été supprimé'), 201);
        }
        catch (Exception $e){
            return response()->json(array('message' => 'Un problème est survénu. Veuillez réessayer dans un instant'), 301);
        }
    }
}
