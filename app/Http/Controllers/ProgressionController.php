<?php

namespace App\Http\Controllers;

use App\Progression;
use App\User;
use Illuminate\Http\Request;

class ProgressionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        dd(User::mesProgressions(1));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Verification de l'existence d'une progression pour ce livre

        $progression = Progression::where('user_id', $request->user_id)
                        ->where('livre_id', $request->livre_id)
                        ->where('langue_id', $request->langue_id)->first();

        if(!is_null($progression)){
            if($request->contenu_id > $progression->contenuè_id){
                $element  = Progression::find($progression->id);
                $element->contenu_id = $request->contenu_id;
                $element->save();
            }
        }else{
            $progression = new Progression();
            $progression->livre_id      = $request->livre_id;
            $progression->contenu_id    = $request->contenu_id;
            $progression->langue_id     = $request->langue_id;
            $progression->user_id       = $request->user_id;
            $progression->save();
        }

        return response()->json(array('message', true));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
