<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    public function categories() {
        return $this->belongsToMany('App\Categorie', 'categories_has_videos', 'video_id', 'category_id');
    }
}
