<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Auteur extends Model
{
    //retourne les livres d'un auteur
    static public function livres($idAuteur) {
        $ids = LivresHasAuteur::where('auteur_id', $idAuteur)->get();
        $result = array();
        foreach ($ids as $element) {
            array_push($result, Livre::find($element));
        }
        return $result;
    }
}
