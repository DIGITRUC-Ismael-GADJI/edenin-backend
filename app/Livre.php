<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Livre extends Model
{
    public function ages(){
        return $this->belongsToMany('App\Age', 'livres_has_ages');
    }

    static public function categories($idBook) {
        $books = LivresHasCategorie::where('livre_id', $idBook)->get();
        $categories = array();
        foreach ($books as $element){
            $categories [] = Categorie::find($element->categorie_id)->toArray();
        }
        return $categories;
    }

    static public function auteurs($idBook) {
        $books = LivresHasAuteur::where('livre_id', $idBook)->get();
        $auteurs = array();
        foreach ($books as $element){
            $auteurs [] = Auteur::find($element->auteur_id)->toArray();
        }
        return $auteurs;
    }

    static public function illustrateurs($idBook) {
        $books = LivreIllustrateur::where('livre_id', $idBook)->get();
        $illustrateurs = array();
        foreach ($books as $element){
            $illustrateurs [] = Illustrateur::find($element->illustrateur_id)->toArray();
        }
        return $illustrateurs;
    }



    public function contenus($langue) {
        if(is_null($langue)){
            return $this->hasMany('App\Contenu')->where('delete', false)->with('textes', 'images', 'audios', 'disposition', 'langue');
        }
        return $this->hasMany('App\Contenu')->where('langue_id', $langue)->where('delete', false)->with('textes', 'images', 'audios', 'disposition');
    }

    public function users() {
        return $this->belongsToMany('App\User', 'users_has_livres');
    }

    static public function getAllLangageForThisBook($idBook){
        $contenus = Contenu::where('livre_id', $idBook)->get();
        $langues = array();
        $temp = array();
        foreach ($contenus as $element) {
            if(!in_array($element->langue_id, $temp)){
                array_push($langues, Langue::find($element->langue_id));
                array_push($temp, $element->langue_id);
            }
        }
        return $langues;
    }

    public function progression() {
        return $this->belongsTo('App\Progression');
    }


}
