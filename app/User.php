<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use PhpParser\Node\Expr\Array_;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function commentaires() {
        return $this->hasMany('App\Commentairelivre');
    }

    public function livres($userId) {
        $books = UsersHasLivre::where('user_id', $userId);
        $livres = array();
        foreach ($books as $book) {
            array_push($livres, array(
                'livre' => Livre::find($book->livre_id),
                'auteurs'   => Livre::auteurs($book->livre_id),
                'categories'    => Livre::categories($book->livre_id),
            ));
        }
        return $livres;
    }

    public function partages() {
        return $this->hasMany('App\Partage')->with('user',
            'usersHasLivres', 'usersHasLivres.livre');
    }


    /**
     * @param $userId //Id de l'utilisateur
     * @return array
     */
    public static function mesProgressions($userId) {
        $progressions = Progression::where('user_id', $userId)->get();

        $result = array();
        foreach ($progressions as $progression){
            $livre = Livre::find($progression->livre_id);
            array_push($result, array(
               'livre'  => $livre->titre,
               'image'  => $livre->picture,
               'langue' => Langue::find($progression->langue_id)->nom,
               'progression' => round(Contenu::find($progression->contenu_id)->position*100/count($livre->contenus($progression->langue)->get()->toArray())) . '%'
            ));
        }
        return $result;
    }
}
